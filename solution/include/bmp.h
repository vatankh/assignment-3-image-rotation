#ifndef IMAGE_ROTATION_H_BMP
#define IMAGE_ROTATION_H_BMP
#define SIGNATURE_BMP 0x4d42
#define PIXEL_BIT_BY_SIZE 24
#define BMP_SIZE_BYTE 40
#define COMPRESSION 0

#include "bmp_status.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *image);
enum write_status to_bmp(FILE *out, struct image *image);
uint32_t calculateImageSize(uint32_t width, uint32_t height);
uint32_t calculateFileSize(uint32_t img_size);
uint32_t calculatePaddding(uint32_t width);
struct bmp_header create_header(struct image* image);



#endif 
