#include "bmp.h"
#include "bmp_status.h"
#include "image.h"
#include <inttypes.h>
#include <stdio.h>

enum {
    BMP_PIXEL_SIZE = sizeof(struct pixel),
    BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

uint32_t calculatePaddding(uint32_t width) {
    return (4 - (width * 3) % 4) % 4;

}
struct bmp_header create_header(struct image* image) {
    const uint32_t image_size = calculateImageSize(image->width, image->height);
    const uint32_t file_size = calculateFileSize(image_size);

    return (struct bmp_header){
                   .bfType = SIGNATURE_BMP,
                   .bfileSize = file_size,
                   .bfReserved = 0,
                   .bOffBits = BMP_HEADER_SIZE,
                   .biSize = BMP_SIZE_BYTE,
                   .biWidth = image->width,
                   .biHeight = image->height,
                   .biPlanes = 1,
                   .biBitCount = PIXEL_BIT_BY_SIZE,
                   .biCompression = COMPRESSION,
                   .biSizeImage = image_size,
                   .biXPelsPerMeter = 0,
                   .biYPelsPerMeter = 0,
                   .biClrUsed = 0,
                   .biClrImportant = 0
               };
}



uint32_t calculateImageSize(uint32_t width, uint32_t height) {
    return (width + calculatePaddding(width)) * BMP_PIXEL_SIZE * height;
}

uint32_t calculateFileSize(uint32_t img_size) {
    return (img_size + BMP_HEADER_SIZE);
}




enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;
   

    if (fread(&header, BMP_HEADER_SIZE, 1, in) != 1) {
        return READ_ERROR_IO;
    }


    if (header.bfType != SIGNATURE_BMP) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != PIXEL_BIT_BY_SIZE) {
        return READ_INVALID_BITS;
    }

    if (header.biSize <= 0) {
        return READ_INVALID_FILE_SIZE;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_IMAGE_SIZE;
    }


    *image = createImage(header.biWidth, header.biHeight);
    
    if (image->data == NULL) {
        return READ_ERROR_D;
    }

    uint32_t padding = calculatePaddding((uint32_t)image->width);


    for (uint64_t i = 0; i < image->height; i++) {
        void *pointerStart = image->data + image->width * i;

        if (fread(pointerStart, BMP_PIXEL_SIZE, image->width, in) != image->width) {
            deleteImage(*image);
            return READ_ERROR_1;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            deleteImage(*image);
            return READ_ERROR_2;
        }
      


    }

    return READ_OK;

}



enum write_status to_bmp(FILE* out, struct image* image) {
    struct bmp_header out_header;

    uint32_t padding = calculatePaddding((uint32_t)image->width);
    
    
    
    

    out_header = create_header(image);
    
    if (fwrite(&out_header, BMP_HEADER_SIZE, 1, out) == 0) {
        return ERROR_WRITE;
    }



    for (uint64_t i = 0; i < image->height; i++) {
        void* pointerStart = (image->data + image->width * i);

        if (fwrite(pointerStart, BMP_PIXEL_SIZE, image->width, out) == 0) {
            deleteImage(*image);
            return ERROR_WRITE;
        }
        uint8_t garb[3] = {0};

        if ((padding != 0) && (!fwrite(&garb, padding, 1, out))) {  
            deleteImage(*image);
            return ERROR_WRITE;
        }
    }
    return WRITE_OK;

    
}
