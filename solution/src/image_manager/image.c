#include "image.h"
#include <stdio.h>

struct image createImage(uint32_t width, uint32_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = (struct pixel *)malloc(sizeof(struct pixel) * width * height);
    if (img.data == NULL) {
        printf("Error allocating memory for image data\n");
        return img;
    }
    return img;
}

void deleteImage(struct image img) {
  free(img.data);
  img.height = 0;
  img.width = 0;
}
