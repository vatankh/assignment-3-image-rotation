#include "file.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

enum state openFile(FILE **f, char const *path, char const *mode) {
  *f = fopen(path, mode);
  return *f == NULL ? ERROR_OPEN : WORKED;
}
void freeFile(FILE **f1, FILE **f2) {
  free(f1);
  free(f2);
}

enum state closeFile(FILE *f) {
  if (fclose(f) == 0) {
    return WORKED;
  } else {
    return ERROR_CLOSE;
  }
}
