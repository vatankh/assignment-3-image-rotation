#include "main.h"
#include "bmp.h"
#include "bmp_status.h"
#include "file.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>

#include <stdlib.h>



int main(int argc, char **argv) {
  struct image old_img;
  struct image rotated_image;
  enum state open_file_status;
  enum state close_file_status;

  if (argc != 3) {
    printf("You didn't pass all the arguments");
    return 1;
  }

  FILE **file_in = malloc(sizeof(FILE *));
  FILE **file_out = malloc(sizeof(FILE *));

  open_file_status = openFile(file_in, argv[1], "rb");
  if (open_file_status == WORKED) {
    printf("Input file open\n");
  } else {
    printf("input file open error");
    freeFile(file_in, file_out);
    return -1;
  }

  open_file_status = openFile(file_out, argv[2], "wb");
  if (open_file_status == WORKED) {
    printf("Output file open\n");
  } else {
    printf("open error");
    freeFile(file_in, file_out);
    return -1;
  }


  enum read_status f_read_status = from_bmp(*file_in, &old_img);
  switch (f_read_status) {
  case READ_ERROR_IO:
    printf("read error bmppp");
    break;
  case READ_ERROR_D:
    printf("read bmp error data");
    break;
  case READ_INVALID_BITS:
    printf("read invalid bits");
    break;
  case READ_ERROR_1:
    printf("read bmp error 1");
    break;
  case READ_ERROR_2:
    printf("read bmp error 2");
    break;
  case READ_INVALID_FILE_SIZE:
    printf("read invalid file size");
    break;
  case READ_INVALID_SIGNATURE:
    printf("read invalid signature");
    break;
  case READ_INVALID_IMAGE_SIZE:
    printf("read invalid image size");
    break;
  case READ_INVALID_HEADER:
    printf("read invalid header");
    break;
  default:
    break;
  }

  if (f_read_status != READ_OK) {
    freeFile(file_in, file_out);
    if (fclose(*file_in) != 0) {
        printf("Error closing input file\n");
    }
    if (fclose(*file_out) != 0) {
        printf("Error closing output file\n");
    }
    return -1;
  }

  rotated_image = rotateImage(old_img);
  printf("image rotated\n");
  

  deleteImage(old_img);

  enum write_status f_write_status = to_bmp(*file_out, &rotated_image);
  if (f_write_status == WRITE_OK) {
    printf("bmp wroten");
  } 
  else {
    deleteImage(rotated_image);
    freeFile(file_in, file_out);
    printf("bmp write error");
    return -1;
  }

  deleteImage(rotated_image);

  close_file_status = closeFile(*file_in);
  if (close_file_status == WORKED) {
    printf("Input file success\n");
  }

  else {
    printf("error file close");
    freeFile(file_in, file_out);
    return -1;
  }

  close_file_status = closeFile(*file_out);

  if (close_file_status == WORKED) {
    printf("Output file success\n");
  }

  else {
    printf("error file close");
    freeFile(file_in, file_out);
    return -1;
  }

  
  
  freeFile(file_in, file_out);

  return 0;
}
